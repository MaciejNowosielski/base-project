using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMovement : MonoBehaviour
{
    private float cubeSpeed = 10.0f;
    Vector3 basePosition;
    private bool collide = false;

    void OnCollisionEnter(Collision col)
    {
        collide = true;
    }

    void OnCollisionExit(Collision col)
    {
        collide = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        basePosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {
            transform.position += cubeSpeed * Time.deltaTime * Vector3.right;
        }

        if (Input.GetKey(KeyCode.A))
        {
            transform.position -= cubeSpeed * Time.deltaTime * Vector3.right;
        }

        if (Input.GetKey(KeyCode.W))
        {
            transform.position += cubeSpeed * Time.deltaTime * Vector3.forward;
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.position -= cubeSpeed * Time.deltaTime * Vector3.forward;
        }

        if (Input.GetKeyDown(KeyCode.Space) && collide)
        {
            GetComponent<Rigidbody>().AddForce(Vector3.up * cubeSpeed * 50.0f);
        }

        if (transform.position.y < -30.0f)
        {
            transform.position = basePosition;
        }
    }
}
