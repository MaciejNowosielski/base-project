using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public Transform cubeTransform;
    public Vector3 moveVector;

    // Start is called before the first frame update
    void Start()
    {
        moveVector = transform.position - cubeTransform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = cubeTransform.position + moveVector;
    }
}
